# SAFT project

Probability distributions of travel time, Eulerian and Lagrangian velocities for a large set of discrete fracture network simulations - from synthetic to field-calibrated models - performed to investigate the factors controlling flow and transport in fractured rocks.

The file contains 
- SAFT2024-analysis.xlsx: fit parameter database
- /BTC: directory containing the travel time probability distributions 
- /velocities: directory containing the probability distributions of the Eulerian and Lagrangian velocities

The probability distributions have been calculated from flow simulations performed by [DFN.lab](https://fractorylab.org/dfnlab-software/). The files are the property of the [Fractory](https://fractorylab.org/) team. 